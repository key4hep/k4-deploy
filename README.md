This repository has the workflows for building the
[key4hep](https://github.com/key4hep) software. This is the repository that
triggers the nightly and release builds and other builds or workflows.

# Workflows

## Builds
There are several builds depending on the operating system and compiler. In
addition, the builds are duplicated because there is the optimized build and the
debug build. The builds and some minimal tests run in the same stage because the
debug builds depend on the optimized builds, so they will only be built if the
optimized build went fine.

We have three worwflows here:
 - Nightly from scratch: Will rebuild all the packages.
 - Nightly: the regular nightly workflow, will reuse the latest build from
   scratch and build on top of it.
 - Release: build a release for /cvmfs/sw.hsf.org, rebuilds all the packages.

## Delete old nightlies
This is a workflow to delete old nightlies from cvmfs. It will find the oldest
build from scratch and will delete all the nightlies that are using that one as
upstream. The workflow prints which nightlies will be deleted and then sleeps
for some time so there is time to cancel the job if what it's going to delete is
not expected.

## Build images
Build docker images that are deployed on gitlab.cern.ch and are used for builds.

## Update setup scripts
Sync the setup scripts on cvmfs to the ones that can be found in the repository
`key4hep-spack` on Github.
